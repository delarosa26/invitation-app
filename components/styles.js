const React = require("react-native");

const { StyleSheet } = React;

export default {
  inlineItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 20,
    alignItems: "center"
  },
  inlineCenterItem: {
    flexDirection: "row",
    justifyContent: "center",
    padding: 20,
    alignItems: "center"
  },
  inlineItem: {
    margin: 0,
    padding: 15
  },
  centerItem:{
    justifyContent:'center',
    alignItems:'center'
  },
  columnItem: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: 'column',
  },
  flag:{
    margin: 10,
    minHeight: 40
  },
  inactiveFlag:{
    opacity: 0.6
  },
  activaFlag:{
    opacity:1,
    borderColor:'#a0915a',
    borderWidth:StyleSheet.hairlineWidth,
    padding:0
  },
  offset:{
    margin:5
  },
  bigFont: {
    fontSize: 25
  },
  rightText: {
    textAlign: 'right',
    marginRight: 10
  },
  centerText: {
    textAlign: 'center'
  },
  content: {
    minHeight: 'auto'
  },
  creamIcon: {
    marginRight: 10,
    color: '#a0915a',
    fontSize: 25
  },
  iconSize: {
    fontSize: 30
  },
  textarea: {
    marginLeft: 10,
    minHeight: 80
  },
  daybtn: {
    borderColor: '#a0915a',
    borderBottomWidth: 2,
    borderTopWidth: 2
  },
  primarycolor: {
    color: '#a0915a'
  },
  primaryBackground: {
    backgroundColor: '#111'
  },
  secondarycolor: {
    color: '#222'
  },
  graycolor: {
    color: '#A9A9A9'
  },
  menuIcon:{
    fontSize: 35,
    color:'#a0915a'
  },
  activemenuIcon:{
    fontSize: 35,
    color:'#222'
  },
  savebtn: {
    borderColor: '#a0915a',
    borderWidth: 2,
    backgroundColor:'#fff'
  },
  primarybtnText:{
    fontWeight: 'bold',
    color:'#a0915a'
  },
  brandLine: {
    borderBottomWidth: 2,
    borderBottomColor: '#a0915a'
  },
  activeDate:{
    borderColor:'#a0915a',
    borderWidth:StyleSheet.hairlineWidth,
  },
  btnText:{
    padding:0,
    minHeight: 75,
    marginLeft:20,
    marginRight:20,
  },
  noteFont:{
    fontSize: 17
  }
}
