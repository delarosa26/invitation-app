import React, {Component} from 'react'
import {StyleSheet, Image, TextInput} from 'react-native';
import {
  Header,
  Container,
  Content,
  Footer,
  Form,
  Item,
  Input,
  Label,
  List,
  Body,
  Title,
  Text,
  Button
} from 'native-base'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import SVGImage from 'react-native-svg-image'

import {
    Alert,
    AsyncStorage,
    View
  } from 'react-native';
  
  import { graphql, gql } from 'react-apollo'

 class LandingComponent extends Component {
    constructor (props) {
        super(props)

    }     
    
    test () {
        Alert.alert(this.state.username)
    }
    
  render() {
      return (
      <Container>
        <Header style={styles.brandLine}>
          <Body>
            <Title style={styles.primarycolor}>
              BIENVENIDO
            </Title>
          </Body>
        </Header>
        <Content>
            <Item style={[styles.inlineCenterItem, {borderColor:'transparent'}]}>
              <Body>
                <SVGImage style={[{
                    width: 320,
                    height: 200
                  }
                ]} source={{
                  uri: 'https://bpprivilegeclub.com/imgs/logo_bppc.svg'
                }}/>
              </Body>
            </Item>
            
              <List style={styles.columnItem}>
                <Item >
                 <Text> Some Text Goes here</Text> 
                </Item>
              </List>
    
        </Content>
        <Footer>
          <Text style={[styles.primarycolor, styles.inlineItem]}>
            © 2017 BAHIA PRINCIPE PRIVILEGE CLUB</Text>
        </Footer>
      </Container>
    )
  }
}

const verifyTokenMutation = gql`
mutation verifyTokenMutation($token: String!) {
    verifyToken(token: $token) {
        User
    }
}
`
export default graphql(verifyTokenMutation)(LandingComponent)
