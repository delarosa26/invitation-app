import React, {Component} from 'react'
import {StyleSheet, Image, TextInput} from 'react-native';
import {
  Header,
  Container,
  Content,
  Footer,
  Form,
  Item,
  Input,
  Label,
  List,
  Body,
  Title,
  Text,
  Button
} from 'native-base'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import SVGImage from 'react-native-svg-image'

import {
    Alert,
    AsyncStorage,
    View
  } from 'react-native';
  
import { graphql, gql } from 'react-apollo'
import LandingComponent  from '../components/LandingComponent'  
import { StackNavigator } from 'react-navigation'
  
 class SignIn extends Component {
     
    constructor (props) {
        super(props)
        this.authenticate = this.authenticate.bind(this)
        this.goToLanding = this.goToLanding.bind(this)


        const Router = StackNavigator({
          Landing: { screen: LandingComponent }
        })

        
        this.state = {
          username: '',
          password: ''
        }
      }     
    
      goToLanding () {
        Alert.alert('now i go...')
        this.props.navigation.navigate('Landing')
      }

      cleanFields () {
        this.setState({ 
          username: '', 
          password: '' 
        })
      }
    
    
      async authenticate () {
        try {
          
          const username = this.state.username.toLowerCase()
          const password = this.state.password.toLowerCase()

          if (!username || !password) {
            Alert.alert('Ambos campos son requeridos')  
            return
          }

          const { mutate, navigate } = this.props
          const res = await mutate({ variables: { username, password } })
          const { token } = res.data.authenticate
          this.goToLanding()
         //Alert.alert(token); 
        } catch (e) {
          const response = e.message;
          
          if (response.indexOf('username or password') > 0) {
            Alert.alert('Código de empleado y/o contraseña incorrectos')
            this.cleanFields()
          }
        }
      }
  render() {
    return (
      <Container >
        <Header style={styles.brandLine}>
          <Body>
            <Title style={styles.primarycolor}>
              INICIO DE SESION
            </Title>
          </Body>
        </Header>
        <Content>
            <Item style={[styles.inlineCenterItem, {borderColor:'transparent'}]}>
              <Body>
                <SVGImage style={[{
                    width: 320,
                    height: 200
                  }
                ]} source={{
                  uri: 'https://bpprivilegeclub.com/imgs/logo_bppc.svg'
                }}/>
              </Body>
            </Item>
            <Form>
              <List style={styles.columnItem}>
                <Item style={[
                  {
                    maxWidth: 300
                  },
                  styles.centerItem
                ]}>
                  <Input placeholder='CODIGO EMPLEADO' onChangeText={(text) => this.setState({ username: text })} value={this.state.username} />
                  <Icon active name='account-circle' style={styles.creamIcon} />
                </Item>
                <Item style={[
                  {
                    maxWidth: 300
                  },
                  styles.centerItem
                ]}>
                  <Input secureTextEntry={true} placeholder='CONTRASEÑA'  onChangeText={(text) => this.setState({ password: text })} value={this.state.password} />
                  <Icon name='lock' style={styles.creamIcon}/>

                </Item>

              </List>
            </Form>
            <Item style={[
              styles.inlineCenterItem, {
                borderBottomWidth: 0
              }
            ]}>
              <Button 
              onPress={this.authenticate}
              iconRight full light style={[styles.loginBtn,
                styles.inlineItem, {
                  minWidth: 300
                }
              ]}>
                <Text style={styles.primarybtnText}>
                  INICIAR
                </Text>
                <Icon style={[
                  styles.creamIcon, {
                    marginRight: 0
                  }
                ]} name="play-arrow"/>
              </Button>
            </Item>
        </Content>
        <Footer>
          <Text style={[styles.primarycolor, styles.inlineItem]}>
            © 2017 BAHIA PRINCIPE PRIVILEGE CLUB</Text>
        </Footer>
      </Container>
    )
  }
}

const authenticateMutation = gql`
mutation authenticateMutation($username: String!, $password: String!) {
  authenticate(username: $username, password: $password) {
    token
  }
}
`
export default graphql(authenticateMutation)(SignIn)
