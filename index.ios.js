import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native'
import {
  ApolloProvider,
  ApolloClient,
  createNetworkInterface
 } from 'react-apollo'
import { StackNavigator } from 'react-navigation'
import HomeScreen from './screens/containers/HomeScreen'
import SigninScreen from './screens/containers/SigninScreen'
import SignupScreen from './screens/containers/SignupScreen'
import EnsureAuth from './auth/containers/EnsureAuth'

import styles from './components/styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import SignIn from './components/signin'
import LandingComponent  from './components/LandingComponent'  

const networkInterface = createNetworkInterface({
  uri: 'http://localhost:3000/graphql',
});

const client = new ApolloClient({ networkInterface })

const Router = StackNavigator({
  SignIn: { screen: SignIn },
  Landing: {screen: LandingComponent}
})

const testApp2 = () => (
  <ApolloProvider client={client}>
      <Router />
  </ApolloProvider>
)

AppRegistry.registerComponent('testApp2', () => testApp2);
